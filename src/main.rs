#[macro_use]
extern crate lalrpop_util;

lalrpop_mod!(#[allow(clippy::all)] pub fml); // load module synthesized by LALRPOP

mod parser;

use std::{env, fs};

use fnv::FnvHashMap;
use thunderdome::{Arena, Index};

use crate::{
    fml::TopLevelParser,
    parser::{Identifier, AST},
};

// LATER more tests - from other students and spec

// TODO all the LATERs
//      They're either things I'd do if I had infinite time
//      or edge cases I wanted to handle but I can plausibly say they didn't occur to me. ;)

#[allow(unused)]
macro_rules! mydbg {
    ( $( $t:tt )* ) => {
        #[cfg(feature = "mydbg")]
        dbg!( $( $t )* );
    }
}

fn main() {
    let mut args = env::args();
    args.next().unwrap(); // skip self
    let path = args.next().expect("Need .fml path as param");
    // LATER take stdin, use rlwrap in scripts
    let text = fs::read_to_string(path).unwrap();
    let mut interpreter = Interpreter::new();
    interpreter.run(&text);
}

#[derive(Debug, Clone)]
struct Interpreter {
    /// The vec's items are (depth of call stack, the actual variables)
    scopes: Vec<(i32, FnvHashMap<String, Value>)>,
    call_depth: i32,
    functions: FnvHashMap<String, (Vec<Identifier>, Box<AST>)>,
    array_heap: Arena<Vec<Value>>,
    object_heap: Arena<Object>,
}

impl Interpreter {
    fn new() -> Self {
        Self {
            scopes: Vec::new(),
            call_depth: 0,
            functions: FnvHashMap::default(),
            array_heap: Arena::new(),
            object_heap: Arena::new(),
        }
    }

    fn push_scope(&mut self, is_call: bool) {
        if is_call {
            self.call_depth += 1;
        }
        self.scopes.push((self.call_depth, FnvHashMap::default()));
    }

    fn pop_scope(&mut self) {
        self.scopes.pop().unwrap();
        if let Some((call_depth, _)) = self.scopes.last() {
            self.call_depth = *call_depth;
        }
    }

    fn scopes_insert(&mut self, key: String, value: Value) {
        self.scopes.last_mut().unwrap().1.insert(key, value);
    }

    fn scopes_get(&self, key: &str) -> Value {
        // All scopes in the current function
        for (depth, scope) in self.scopes.iter().rev() {
            if *depth != self.call_depth {
                break;
            }
            // LATER perf
            if scope.contains_key(key) {
                return *scope.get(key).unwrap();
            }
        }

        // The global scope (but not blocks inside it)
        let (_depth, scope) = self.scopes.first().unwrap();
        if scope.contains_key(key) {
            return *scope.get(key).unwrap();
        }

        panic!("Key {} not found", key);
    }

    fn scopes_update(&mut self, key: String, value: Value) {
        // All scopes in the current function
        for (depth, scope) in self.scopes.iter_mut().rev() {
            if *depth != self.call_depth {
                break;
            }
            // LATER perf
            if scope.contains_key(&key) {
                scope.insert(key, value);
                return;
            }
        }

        // The global scope (but not blocks inside it)
        let (_depth, scope) = self.scopes.first_mut().unwrap();
        if scope.contains_key(&key) {
            scope.insert(key, value);
            return;
        }

        panic!("Key {} not found", key);
    }

    fn run(&mut self, text: &str) {
        let ast: AST = TopLevelParser::new().parse(text).expect("Parse error");
        mydbg!(&ast);
        self.eval(&ast);
    }

    fn eval(&mut self, ast: &AST) -> Value {
        match ast {
            AST::Integer(val) => Value::Integer(*val),
            AST::Boolean(val) => Value::Boolean(*val),
            AST::Null => Value::Null,

            AST::Variable { name, value } => {
                let val = self.eval(value);
                self.scopes_insert(name.0.clone(), val);
                val
            }
            AST::Array { size, value } => {
                if let Value::Integer(size) = self.eval(size) {
                    let data = (0..size).map(|_| self.eval(value)).collect();
                    let handle = self.array_heap.insert(data);
                    Value::ArrayRef(handle)
                } else {
                    panic!("Size is not an Integer: {:?}", size);
                }
            }
            AST::Object { extends, members } => {
                let parent = self.eval(extends);
                let mut object = Object {
                    parent,
                    fields: FnvHashMap::default(),
                    methods: FnvHashMap::default(),
                };

                for member in members {
                    match &**member {
                        AST::Variable { name, value } => {
                            let prev = object.fields.insert(name.0.clone(), self.eval(value));
                            assert_eq!(prev, None);
                        }
                        AST::Function {
                            name,
                            parameters,
                            body,
                        } => {
                            object
                                .methods
                                .insert(name.0.clone(), (parameters.clone(), body.clone()));
                        }
                        _ => unreachable!("Objects can only have fields and methods, i think"),
                    }
                }

                let object_handle = self.object_heap.insert(object);
                Value::ObjectRef(object_handle)
            }

            AST::AccessVariable { name } => self.scopes_get(name.as_str()),
            AST::AccessField { object, field } => {
                if let Value::ObjectRef(handle) = self.eval(object) {
                    *self
                        .object_heap
                        .get(handle)
                        .expect("Object not found")
                        .fields
                        .get(field.as_str())
                        .expect(&format!("Field not found: {}", field.as_str()))
                } else {
                    panic!("AccessField on non-object: {:?}", object);
                }
            }
            AST::AccessArray { array, index } => self.eval(&AST::call_method(
                (**array).clone(),
                "get".into(),
                vec![(**index).clone()],
            )),

            AST::AssignVariable { name, value } => {
                let val = self.eval(value);
                self.scopes_update(name.0.clone(), val);
                val
            }
            AST::AssignField {
                object,
                field,
                value,
            } => {
                let val = self.eval(value);
                if let Value::ObjectRef(handle) = self.eval(object) {
                    *self
                        .object_heap
                        .get_mut(handle)
                        .expect("Object not found")
                        .fields
                        .get_mut(field.as_str())
                        .expect(&format!("Field not found: {}", field.as_str())) = val;
                    val
                } else {
                    panic!("AccessField on non-object: {:?}", object);
                }
            }

            AST::AssignArray {
                array,
                index,
                value,
            } => self.eval(&AST::call_method(
                (**array).clone(),
                "set".into(),
                vec![(**index).clone(), (**value).clone()],
            )),

            AST::Function {
                name,
                parameters,
                body,
            } => {
                self.functions
                    .insert(name.0.clone(), (parameters.clone(), body.clone()));
                Value::Null
            }

            AST::CallFunction { name, arguments } => {
                let function = self.functions.get(name.as_str()).unwrap().clone();
                assert_eq!(function.0.len(), arguments.len());

                let args: Vec<_> = arguments
                    .iter()
                    .map(|argument| self.eval(argument))
                    .collect();

                self.push_scope(true);
                for (param, arg) in function.0.iter().zip(args) {
                    self.scopes_insert(param.0.clone(), arg);
                }

                let ret = self.eval(&function.1);

                self.pop_scope();

                ret
            }
            AST::CallMethod {
                object,
                name,
                arguments,
            } => {
                // LATER test different expressions
                // LATER the error messages are probably msleadingwhen inheriting from primitives and calling nonexisting methods
                let mut receiver = self.eval(object);

                let args: Vec<_> = arguments
                    .iter()
                    .map(|argument| self.eval(argument))
                    .collect();

                self.push_scope(true);

                let ret = loop {
                    mydbg!(receiver, name, &args);
                    match receiver {
                        Value::Integer(obj) => {
                            break if let Value::Integer(arg) = args[0] {
                                match name.as_str() {
                                    "+" => Value::Integer(obj + arg),
                                    "-" => Value::Integer(obj - arg),
                                    "*" => Value::Integer(obj * arg),
                                    "/" => Value::Integer(obj / arg),
                                    "%" => Value::Integer(obj % arg),
                                    "<=" => Value::Boolean(obj <= arg),
                                    ">=" => Value::Boolean(obj >= arg),
                                    "<" => Value::Boolean(obj < arg),
                                    ">" => Value::Boolean(obj > arg),
                                    "==" => Value::Boolean(obj == arg),
                                    "!=" => Value::Boolean(obj != arg),
                                    _ => panic!("Unknown method on Integer: {}", name.as_str()),
                                }
                            } else {
                                panic!("RHS must be Integer, got: {}", self.stringify(args[0]))
                            }
                        }
                        Value::Boolean(obj) => {
                            break if let Value::Boolean(arg) = args[0] {
                                match name.as_str() {
                                    "&" => Value::Boolean(obj & arg),
                                    "|" => Value::Boolean(obj | arg),
                                    "==" => Value::Boolean(obj == arg),
                                    "!=" => Value::Boolean(obj != arg),
                                    _ => panic!("Unknown method on Boolean: {}", name.as_str()),
                                }
                            } else {
                                panic!("RHS must be Boolean, got: {}", self.stringify(args[0]))
                            }
                        }
                        Value::Null => {
                            let eq = args[0] == Value::Null;
                            break match name.as_str() {
                                "==" => Value::Boolean(eq),
                                "!=" => Value::Boolean(!eq),
                                _ => panic!("Unknown method on null: {}", name.as_str()),
                            };
                        }
                        Value::ArrayRef(array_handle) => match name.as_str() {
                            "get" => {
                                break if let Value::Integer(pos) = args[0] {
                                    self.array_heap[array_handle][pos as usize]
                                } else {
                                    panic!("Index must be an Integer");
                                }
                            }
                            "set" => {
                                break if let Value::Integer(pos) = args[0] {
                                    self.array_heap[array_handle][pos as usize] = args[1];
                                    args[1]
                                } else {
                                    panic!("Index must be an Integer");
                                }
                            }
                            _ => panic!("Unknown method on array: {}", name.as_str()),
                        },
                        Value::ObjectRef(object_handle) => {
                            let object = &self.object_heap[object_handle];
                            let parent = object.parent;
                            if let Some(method) = object.methods.get(name.as_str()) {
                                let method = method.clone(); // borrowck
                                self.scopes_insert("this".to_owned(), receiver);
                                for (param, arg) in method.0.iter().zip(args) {
                                    self.scopes_insert(param.0.clone(), arg);
                                }

                                break self.eval(&method.1);
                            }

                            receiver = parent;
                        }
                    }
                };

                self.pop_scope();

                ret
            }

            AST::Top(nodes) | AST::Block(nodes) => {
                self.push_scope(false);
                let mut ret = Value::Null;
                for node in nodes {
                    ret = self.eval(node);
                }
                self.pop_scope();
                ret
            }
            AST::Loop { condition, body } => {
                // This comment forces rustfmt to put the `loop` on a new line.
                loop {
                    let cond = self.eval(condition);
                    if cond.falsy() {
                        break Value::Null;
                    }
                    self.eval(body);
                }
            }
            AST::Conditional {
                condition,
                consequent,
                alternative,
            } => {
                let cond = self.eval(condition);
                if cond.truthy() {
                    self.eval(consequent)
                } else {
                    self.eval(alternative)
                }
            }

            AST::Print { format, arguments } => {
                let mut res = String::new();
                let mut escape = false;
                let mut args = arguments.iter();
                for c in format.chars() {
                    if escape {
                        match c {
                            '"' => res.push('"'),
                            '\\' => res.push('\\'),
                            'n' => res.push('\n'),
                            't' => res.push('\t'),
                            '~' => res.push('~'),
                            c => panic!("Bad escape: {}", c),
                        }
                        escape = false;
                    } else {
                        match c {
                            '\\' => escape = true,
                            '~' => {
                                let val = self.eval(args.next().unwrap());
                                res.push_str(&self.stringify(val));
                            }
                            c => res.push(c),
                        }
                    }
                }
                assert_eq!(args.next(), None);
                print!("{}", res);
                Value::Null
            }
        }
    }

    fn stringify(&self, value: Value) -> String {
        match value {
            Value::Integer(val) => val.to_string(),
            Value::Boolean(val) => val.to_string(),
            Value::Null => "null".to_owned(),
            Value::ArrayRef(data_handle) => {
                let mut s = "[".to_owned();
                let mut first = true;
                for value in &self.array_heap[data_handle] {
                    if !first {
                        s.push_str(", ");
                    }
                    first = false;
                    s.push_str(&self.stringify(*value));
                }
                s.push(']');
                s
            }
            Value::ObjectRef(parent_handle) => {
                let mut s = "object(".to_owned();
                let mut first = true;
                let object = &self.object_heap[parent_handle];

                if object.parent != Value::Null {
                    s.push_str("..=");
                    s.push_str(&self.stringify(object.parent));
                    first = false;
                }

                for (field, value) in &object.fields {
                    if !first {
                        s.push_str(", ");
                    }
                    first = false;
                    s.push_str(&field);
                    s.push('=');
                    s.push_str(&self.stringify(*value));
                }

                s.push(')');
                s
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Value {
    Integer(i32),
    Boolean(bool),
    Null,
    ArrayRef(Index),
    ObjectRef(Index),
}

impl Value {
    fn truthy(self) -> bool {
        match self {
            Self::Integer(_) => true,
            Self::Boolean(v) => v,
            Self::Null => false,
            Self::ArrayRef(..) => true,
            Self::ObjectRef(..) => true,
        }
    }

    fn falsy(self) -> bool {
        !self.truthy()
    }
}

#[derive(Debug, Clone)]
struct Object {
    parent: Value,
    fields: FnvHashMap<String, Value>,
    methods: FnvHashMap<String, (Vec<Identifier>, Box<AST>)>,
}
